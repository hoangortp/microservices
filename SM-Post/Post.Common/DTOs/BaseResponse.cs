﻿namespace Post.Common.DTOs;

public class BaseResponse
{
    #region Properties

    public string? Message { get; set; }

    #endregion
}