using CQRS.Core.Events;

namespace Post.Common.Events
{
    public class CommentUpdatedEvent : BaseEvent
    {
        #region Constructors

        public CommentUpdatedEvent() : base(nameof(CommentUpdatedEvent))
        {
        }

        #endregion

        #region Properties

        public Guid CommentId { get; set; }
        
        public string? Comment { get; set; }
        
        public string? Username { get; set; }
        
        public DateTime EditDate { get; set; }

        #endregion
    }
}