using CQRS.Core.Events;

namespace Post.Common.Events
{
    public class MessageUpdatedEvent : BaseEvent
    {
        #region Constructors

        public MessageUpdatedEvent() : base(nameof(MessageUpdatedEvent))
        {
        }

        #endregion

        #region Properties

        public string? Message { get; set; }

        #endregion
    }
}