using CQRS.Core.Events;

namespace Post.Common.Events
{
    public class PostCreatedEvent : BaseEvent
    {
        #region Constructors

        public PostCreatedEvent() : base(nameof(PostCreatedEvent))
        {
        }

        #endregion

        #region Properties

        public string? Author { get; set; }
        
        public string? Message { get; set; }
        
        public DateTime DatePosted { get; set; }

        #endregion
    }
}