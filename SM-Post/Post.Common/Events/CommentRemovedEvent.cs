using CQRS.Core.Events;

namespace Post.Common.Events
{
    public class CommentRemovedEvent : BaseEvent
    {
        #region Constructors

        public CommentRemovedEvent() : base(nameof(CommentRemovedEvent))
        {
        }

        #endregion

        #region Properties

        public Guid CommentId { get; set; }

        #endregion
    }
}