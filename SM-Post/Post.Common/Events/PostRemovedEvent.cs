using CQRS.Core.Events;

namespace Post.Common.Events
{
    public class PostRemovedEvent : BaseEvent
    {
        #region Constructors

        public PostRemovedEvent() : base(nameof(PostRemovedEvent))
        {
        }

        #endregion
    }
}