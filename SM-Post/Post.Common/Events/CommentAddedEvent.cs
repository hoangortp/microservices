using CQRS.Core.Events;

namespace Post.Common.Events
{
    public class CommentAddedEvent : BaseEvent
    {
        #region Constructors

        public CommentAddedEvent() : base(nameof(CommentAddedEvent))
        {
        }

        #endregion

        #region Properties

        public Guid CommentId { get; set; }
        
        public string? Comment { get; set; }
        
        public string? Username { get; set; }
        
        public DateTime CommentDate { get; set; }

        #endregion
    }
}