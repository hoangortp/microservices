using CQRS.Core.Events;

namespace Post.Common.Events
{
    public class PostLikedEvent : BaseEvent
    {
        #region Constructors

        public PostLikedEvent() : base(nameof(PostLikedEvent))
        {
        }

        #endregion
    }
}