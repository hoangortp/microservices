using CQRS.Core.Commands;

namespace Post.Cmd.Api.Commands
{
    public class DeletePostCommand : BaseCommand
    {
        #region Properties

        public string? Username { get; set; }

        #endregion
    }
}