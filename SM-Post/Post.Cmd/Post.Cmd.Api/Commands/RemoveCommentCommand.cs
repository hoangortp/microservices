using CQRS.Core.Commands;

namespace Post.Cmd.Api.Commands
{
    public class RemoveCommentCommand : BaseCommand
    {
        #region Properties

        public Guid CommentId { get; set; }
        
        public string? Username { get; set; }

        #endregion
    }
}