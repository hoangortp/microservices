using CQRS.Core.Commands;

namespace Post.Cmd.Api.Commands
{
    public class NewPostCommand : BaseCommand
    {
        #region Properties

        public string? Author { get; set; }
        
        public string? Message { get; set; }

        #endregion
    }
}