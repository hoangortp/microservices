using CQRS.Core.Commands;

namespace Post.Cmd.Api.Commands
{
    public class EditMessageCommand : BaseCommand
    {
        #region Properties

        public string? Message { get; set; }

        #endregion
    }
}