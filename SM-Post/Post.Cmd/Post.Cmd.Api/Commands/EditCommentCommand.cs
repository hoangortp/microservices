using CQRS.Core.Commands;

namespace Post.Cmd.Api.Commands
{
    public class EditCommentCommand : BaseCommand
    {
        #region Properties

        public Guid CommentId { get; set; }
        
        public string? Comment { get; set; }
        
        public string? Username { get; set; }

        #endregion
    }
}