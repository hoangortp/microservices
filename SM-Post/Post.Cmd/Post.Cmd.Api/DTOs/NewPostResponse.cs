﻿using Post.Common.DTOs;

namespace Post.Cmd.Api.DTOs;

public class NewPostResponse : BaseResponse
{
    #region Properties

    public Guid Id { get; set; }

    #endregion
}