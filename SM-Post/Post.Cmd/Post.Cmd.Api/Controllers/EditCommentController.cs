﻿using CQRS.Core.Exceptions;
using CQRS.Core.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Post.Cmd.Api.Commands;
using Post.Common.DTOs;

namespace Post.Cmd.Api.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class EditCommentController : ControllerBase
{
    #region Fields

    private readonly ILogger<EditCommentController> _logger;
    private readonly ICommandDispatcher _commandDispatcher;

    #endregion

    #region Constructors

    public EditCommentController(ILogger<EditCommentController> logger, ICommandDispatcher commandDispatcher)
    {
        _logger = logger;
        _commandDispatcher = commandDispatcher;
    }

    #endregion

    #region Methods

    [HttpPut("{id}")]
    public async Task<ActionResult> EditCommentAsync(Guid id, EditCommentCommand command)
    {
        try
        {
            command.Id = id;

            await _commandDispatcher.SendAsync(command);

            return Ok(new BaseResponse
            {
                Message = "Edit comment successfully!"
            });
        }
        catch (InvalidOperationException e)
        {
            _logger.Log(LogLevel.Warning, e, "Bad Request!");

            return BadRequest(new BaseResponse()
            {
                Message = e.Message
            });
            ;
        }
        catch (AggregateNotFoundException e)
        {
            _logger.Log(LogLevel.Warning, e, "Not found due to invalid Id!");

            return BadRequest(new BaseResponse()
            {
                Message = e.Message
            });
            ;
        }
        catch (Exception e)
        {
            const string SAFE_ERROR_MESSAGE = "Error occurs while editing comment to the post!";

            _logger.Log(LogLevel.Error, e, SAFE_ERROR_MESSAGE);

            return StatusCode(StatusCodes.Status500InternalServerError, new BaseResponse()
            {
                Message = SAFE_ERROR_MESSAGE
            });
        }
    }

    #endregion
}