﻿using CQRS.Core.Exceptions;
using CQRS.Core.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Post.Cmd.Api.Commands;
using Post.Common.DTOs;

namespace Post.Cmd.Api.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class DeletePostController : ControllerBase
{
    #region Fields

    private readonly ILogger<DeletePostController> _logger;
    private readonly ICommandDispatcher _commandDispatcher;

    #endregion

    #region Constructors

    public DeletePostController(ILogger<DeletePostController> logger, ICommandDispatcher commandDispatcher)
    {
        _logger = logger;
        _commandDispatcher = commandDispatcher;
    }

    #endregion

    #region Methods

    [HttpDelete("{id}")]
    public async Task<ActionResult> DeletePostAsync(Guid id, DeletePostCommand command)
    {
        try
        {
            command.Id = id;

            await _commandDispatcher.SendAsync(command);

            return Ok(new BaseResponse
            {
                Message = "Delete post successfully!"
            });
        }
        catch (InvalidOperationException e)
        {
            _logger.Log(LogLevel.Warning, e, "Bad Request!");
            
            return BadRequest(new BaseResponse()
            {
                Message = e.Message
            });
            ;
        }
        catch (AggregateNotFoundException e)
        {
            _logger.Log(LogLevel.Warning, e, "Not found due to invalid Id!");
            
            return BadRequest(new BaseResponse()
            {
                Message = e.Message
            });
            ;
        }
        catch (Exception e)
        {
            const string SAFE_ERROR_MESSAGE = "Error occurs while deleting the post!";
            
            _logger.Log(LogLevel.Error, e, SAFE_ERROR_MESSAGE);

            return StatusCode(StatusCodes.Status500InternalServerError, new BaseResponse()
            {
                Message = SAFE_ERROR_MESSAGE
            });
        }
    }

    #endregion
}