﻿using CQRS.Core.Exceptions;
using CQRS.Core.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Post.Cmd.Api.Commands;
using Post.Common.DTOs;

namespace Post.Cmd.Api.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class LikePostController : ControllerBase
{
    #region Fields

    private readonly ILogger<LikePostController> _logger;
    private readonly ICommandDispatcher _commandDispatcher;

    #endregion

    #region Constructors

    public LikePostController(ILogger<LikePostController> logger, ICommandDispatcher commandDispatcher)
    {
        _logger = logger;
        _commandDispatcher = commandDispatcher;
    }

    #endregion

    #region Methods

    [HttpPut("{id}")]
    public async Task<ActionResult> LikePostAsync(Guid id)
    {
        try
        {
            await _commandDispatcher.SendAsync(new LikePostCommand { Id = id });

            return Ok(new BaseResponse
            {
                Message = "Like post successfully!"
            });
        }
        catch (InvalidOperationException e)
        {
            _logger.Log(LogLevel.Warning, e, "Bad Request!");

            return BadRequest(new BaseResponse()
            {
                Message = e.Message
            });
            ;
        }
        catch (AggregateNotFoundException e)
        {
            _logger.Log(LogLevel.Warning, e, "Not found due to invalid Id!");

            return BadRequest(new BaseResponse()
            {
                Message = e.Message
            });
            ;
        }
        catch (Exception e)
        {
            const string SAFE_ERROR_MESSAGE = "Error occurs while processing request to like the post!";

            _logger.Log(LogLevel.Error, e, SAFE_ERROR_MESSAGE);

            return StatusCode(StatusCodes.Status500InternalServerError, new BaseResponse()
            {
                Message = SAFE_ERROR_MESSAGE
            });
        }
    }

    #endregion
}