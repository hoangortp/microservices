﻿using CQRS.Core.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Post.Cmd.Api.Commands;
using Post.Common.DTOs;

namespace Post.Cmd.Api.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class RestoreReadDbController : ControllerBase
{
    #region Fields

    private readonly ILogger<RestoreReadDbController> _logger;
    private readonly ICommandDispatcher _commandDispatcher;

    #endregion

    #region Constructors

    public RestoreReadDbController(ILogger<RestoreReadDbController> logger, ICommandDispatcher commandDispatcher)
    {
        _logger = logger;
        _commandDispatcher = commandDispatcher;
    }

    #endregion

    #region Methods

    [HttpPost]
    public async Task<ActionResult> RestoreReadDbAsync()
    {
        try
        {
            await _commandDispatcher.SendAsync(new RestoreReadDbCommand());

            return StatusCode(StatusCodes.Status201Created, new BaseResponse
            {
                Message = "Restore read-database successfully!"
            });
        }
        catch (InvalidOperationException e)
        {
            _logger.Log(LogLevel.Warning, e, "Bad Request!");

            return BadRequest(new BaseResponse()
            {
                Message = e.Message
            });
        }
        catch (Exception e)
        {
            const string SAFE_ERROR_MESSAGE = "Error occurs while restoring the read-database!";

            _logger.Log(LogLevel.Error, e, SAFE_ERROR_MESSAGE);

            return StatusCode(StatusCodes.Status500InternalServerError, new BaseResponse()
            {
                Message = SAFE_ERROR_MESSAGE
            });
        }
    }

    #endregion
}