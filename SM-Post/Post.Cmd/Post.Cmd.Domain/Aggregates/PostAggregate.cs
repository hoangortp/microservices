using CQRS.Core.Domain;
using Post.Common.Events;

namespace Post.Cmd.Domain.Aggregates
{
    public class PostAggregate : AggregateRoot
    {
        #region Fields
        
        private bool _active;
        private string? _author;
        private readonly Dictionary<Guid, Tuple<string, string>> _comments = [];
        
        #endregion

        #region Properties
        
        public bool Active
        {
            get => _active; set => _active = value;
        }
        
        #endregion

        #region Constructors
        
        /// <summary>
        /// Empty constructor
        /// </summary>
        public PostAggregate()
        {

        }

        /// <summary>
        /// Constructor raises event that creates a new aggregate instance which is PostCreatedEvent instance
        /// </summary>
        public PostAggregate(Guid id, string author, string message)
        {
            RaiseEvent(new PostCreatedEvent
            {
                Id = id,
                Author = author,
                Message = message,
                DatePosted = DateTime.Now
            });
        }
        
        #endregion

        #region Methods
        
        /// <summary>
        /// Altering the state of the aggregate
        /// </summary>
        /// <param name="event">A PostCreatedEvent event to create new post</param>
        public void Apply(PostCreatedEvent @event)
        {
            _id = @event.Id;
            _active = true;
            _author = @event.Author;
        }

        /// <summary>
        /// Edit message
        /// </summary>
        /// <param name="message">Message to be edited</param>
        public void EditMessage(string message)
        {
            if (!_active)
            {
                throw new InvalidOperationException("You cannot edit the message of an inactive post!");
            }

            if (string.IsNullOrWhiteSpace(message))
            {
                throw new InvalidOperationException($"The value of {nameof(message)} cannot be null or empty. Please provide a valid {nameof(message)}!");
            }

            RaiseEvent(new MessageUpdatedEvent
            {
                Id = _id,
                Message = message
            });
        }

        /// <summary>
        /// Altering the state of the aggregate
        /// </summary>
        /// <param name="event">A MessageUpdatedEvent event to update message</param>
        public void Apply(MessageUpdatedEvent @event)
        {
            _id = @event.Id;
        }

        /// <summary>
        /// Raising event that creates a new aggregate instance which is PostLikedEvent instance
        /// </summary>
        public void LikePost()
        {
            if (!_active)
            {
                throw new InvalidOperationException("You cannot like an inactive post!");
            }

            RaiseEvent(new PostLikedEvent
            {
                Id = _id
            });
        }

        /// <summary>
        /// Altering the state of the aggregate (Like the post)
        /// </summary>
        public void Apply(PostLikedEvent @event)
        {
            _id = @event.Id;
        }

        /// <summary>
        /// Raising event that creates a new aggregate instance which is CommentAddedEvent instance
        /// </summary>
        /// <param name="comment">New comment content</param>
        /// <param name="username">Username of user who add comment</param>
        public void AddComment(string comment, string username)
        {
            if (!_active)
            {
                throw new InvalidOperationException("You cannot add comment to an inactive post!");
            }

            if (string.IsNullOrWhiteSpace(comment))
            {
                throw new InvalidOperationException($"The value of {nameof(comment)} cannot be null or empty. Please provide a valid {nameof(comment)}!");
            }

            RaiseEvent(new CommentAddedEvent
            {
                Id = _id,
                CommentId = Guid.NewGuid(),
                Comment = comment,
                Username = username,
                CommentDate = DateTime.Now
            });
        }

        /// <summary>
        /// Altering the state of the aggregate
        /// </summary>
        /// <param name="event">A CommentAddedEvent event to add new comment</param>
        public void Apply(CommentAddedEvent @event)
        {
            _id = @event.Id;
            _comments.Add(@event.CommentId, new Tuple<string, string>(@event.Comment!, @event.Username!));
        }


        /// <summary>
        /// Edit comment of the post
        /// </summary>
        /// <param name="commentId">Edit comment based on Id</param>
        /// <param name="comment">Edited comment content</param>
        /// <param name="username">Username of user who edits the comment</param>
        public void EditComment(Guid commentId, string comment, string username)
        {
            if (!_active)
            {
                throw new InvalidOperationException("You cannot edit comment of an inactive post!");
            }

            if (!_comments[commentId].Item2.Equals(username, StringComparison.CurrentCultureIgnoreCase))
            {
                throw new InvalidOperationException("You are not allowed to edit this comment that was made by other user!");
            }

            RaiseEvent(new CommentUpdatedEvent
            {
                Id = _id,
                CommentId = commentId,
                Comment = comment,
                Username = username,
                EditDate = DateTime.Now
            });
        }

        /// <summary>
        /// Altering the state of event
        /// </summary>
        /// <param name="event">A CommentUpdatedEvent event to update comment</param>
        public void Apply(CommentUpdatedEvent @event)
        {
            _id = @event.Id;
            _comments[@event.CommentId] = new Tuple<string, string>(@event.Comment!, @event.Username!);
        }

        /// <summary>
        /// Remove comment of the post 
        /// </summary>
        /// <param name="commentId">Removr comment based on Id</param>
        /// <param name="username">Username of user who removes the comment</param>
        public void RemoveComment(Guid commentId, string username)
        {
            if (!_active)
            {
                throw new InvalidOperationException("You cannot remove comment of an inactive post!");
            }

            if (!_comments[commentId].Item2.Equals(username, StringComparison.CurrentCultureIgnoreCase))
            {
                throw new InvalidOperationException("You are not allowed to remove this comment that was made by other user!");
            }

            RaiseEvent(new CommentRemovedEvent
            {
                Id = _id,
                CommentId = commentId
            });
        }

        /// <summary>
        /// Altering the state of the aggregate
        /// </summary>
        /// <param name="event">A CommentRemovedEvent event to remove the comment</param>
        public void Apply(CommentRemovedEvent @event)
        {
            _id = @event.Id;
            _comments.Remove(@event.CommentId);
        }

        /// <summary>
        /// Delete the post
        /// </summary>
        /// <param name="username">Username of user who deletes the post</param>
        public void DeletePost(string username)
        {
            if (!_active)
            {
                throw new InvalidOperationException("The post has already been removed!");
            }

            if (!_author!.Equals(username, StringComparison.CurrentCultureIgnoreCase))
            {
                throw new InvalidOperationException("You are not allowed to delete the post was made by other user");
            }

            RaiseEvent(new PostRemovedEvent
            {
                Id = _id
            });
        }

        /// <summary>
        /// Alter the state of aggregate
        /// </summary>
        /// <param name="event">A PostRemovedEvent event to remove the post</param>
        public void Apply(PostRemovedEvent @event)
        {
            _id = @event.Id;
            _active = false;
        }

        #endregion
    }
}