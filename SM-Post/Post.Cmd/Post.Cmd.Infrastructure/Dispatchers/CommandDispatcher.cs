using CQRS.Core.Commands;
using CQRS.Core.Infrastructure;

namespace Post.Cmd.Infrastructure.Dispatchers
{
    /// <summary>
    /// This is Concrete Mediator that implements Mediator interface which is ICommandDispatcher
    /// </summary>
    public class CommandDispatcher : ICommandDispatcher
    {
        #region Fields
        
        /// <summary>
        /// A dictionary that holds all the command handling methods as delegate function 
        /// </summary>
        /// <remarks>
        /// The key of the dictionary is the type of command handler
        /// The value of the dictionary is Func with input as base command and output as async task
        /// </remarks>
        private readonly Dictionary<Type, Func<BaseCommand, Task>> _handlers = [];
        
        #endregion

        #region Methods
        
        /// <summary>
        /// The concrete RegisterHandler method
        /// </summary>
        public void RegisterHandler<T>(Func<T, Task> handler) where T : BaseCommand
        {
            // Checking if the command handler already registered by checking
            // if the key of T (as concrete command object) exists in dictionary _handlers
            if (_handlers.ContainsKey(typeof(T)))
            {
                throw new IndexOutOfRangeException("You cannot register the same command handler twice!");
            }

            _handlers.Add(typeof(T), x => handler((T)x));
        }

        /// <summary>
        /// The concrete SendAsync method
        /// </summary>
        /// <param name="command">Command object will be sent/regitered to a command handler</param>
        /// <exception cref="ArgumentException"></exception>
        public async Task SendAsync(BaseCommand command)
        {
            if (_handlers.TryGetValue(command.GetType(), out Func<BaseCommand, Task>? handler))
            {
                await handler(command);
            }
            else
            {
                throw new ArgumentException(nameof(handler), "No command handler was registered");
            }
        }
        
        #endregion
    }
}