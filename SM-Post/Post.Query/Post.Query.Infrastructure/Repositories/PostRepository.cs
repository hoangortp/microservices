﻿using Microsoft.EntityFrameworkCore;
using Post.Query.Domain.Entities;
using Post.Query.Domain.Repositories;
using Post.Query.Infrastructure.DataAccess;

namespace Post.Query.Infrastructure.Repositories;

public class PostRepository : IPostRepository
{
    #region Fields

    private readonly DatabaseContextFactory _contextFactory;

    #endregion

    #region Constructors

    public PostRepository(DatabaseContextFactory contextFactory)
    {
        _contextFactory = contextFactory;
    }

    #endregion

    #region Methods

    public async Task CreateAsync(PostEntity post)
    {
        await using DatabaseContext context = _contextFactory.CreateDbContext();
        
        await context.Posts.AddAsync(post);
        await context.SaveChangesAsync();
    }

    public async Task UpdateAsync(PostEntity post)
    {
        await using DatabaseContext context = _contextFactory.CreateDbContext();

        context.Posts.Update(post);
        await context.SaveChangesAsync();
    }

    public async Task DeleteAsync(Guid postId)
    {
        await using DatabaseContext context = _contextFactory.CreateDbContext();

        var post = await GetByIdAsync(postId);

        context.Posts.Remove(post);
        await context.SaveChangesAsync();
    }

    public async Task<PostEntity> GetByIdAsync(Guid postId)
    {
        await using DatabaseContext context = _contextFactory.CreateDbContext();

        return await context.Posts.Include(p => p.Comments).FirstOrDefaultAsync(p => p.PostId == postId) 
                     ?? throw new Exception("Not Found!");
    }

    public async Task<List<PostEntity>> ListAllAsync()
    {
        await using DatabaseContext context = _contextFactory.CreateDbContext();

        return await context.Posts.AsNoTracking()
                                  .Include(p => p.Comments).AsNoTracking()
                                  .ToListAsync();
    }

    public async Task<List<PostEntity>> ListByAuthorAsync(string author)
    {
        await using DatabaseContext context = _contextFactory.CreateDbContext();
        
        return await context.Posts.AsNoTracking()
                                  .Include(p => p.Comments).AsNoTracking()
                                  .Where(p => p.Author != null && p.Author.Contains(author))
                                  .ToListAsync();
    }

    public async Task<List<PostEntity>> ListWithLikesAsync(int numberOfLikes)
    {
        await using DatabaseContext context = _contextFactory.CreateDbContext();
        
        return await context.Posts.AsNoTracking()
                                  .Include(p => p.Comments).AsNoTracking()
                                  .Where(p => p.Likes >= numberOfLikes)
                                  .ToListAsync();
    }

    public async Task<List<PostEntity>> ListWithCommentsAsync()
    {
        await using DatabaseContext context = _contextFactory.CreateDbContext();
        
        return await context.Posts.AsNoTracking()
                                  .Include(p => p.Comments).AsNoTracking()
                                  .Where(p => p.Comments != null && p.Comments.Count != 0)
                                  .ToListAsync();
    }

    #endregion
}