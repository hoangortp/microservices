﻿using Microsoft.EntityFrameworkCore;
using Post.Query.Domain.Entities;
using Post.Query.Domain.Repositories;
using Post.Query.Infrastructure.DataAccess;

namespace Post.Query.Infrastructure.Repositories;

public class CommentRepository : ICommentRepository
{
    #region Fields

    private readonly DatabaseContextFactory _contextFactory;

    #endregion

    #region Constructors

    public CommentRepository(DatabaseContextFactory contextFactory)
    {
        _contextFactory = contextFactory;
    }

    #endregion

    #region Methods

    public async Task CreateAsync(CommentEntity comment)
    {
        await using var context = _contextFactory.CreateDbContext();

        await context.Comments.AddAsync(comment);
        await context.SaveChangesAsync();
    }

    public async Task UpdateAsync(CommentEntity comment)
    {
        await using var context = _contextFactory.CreateDbContext();

        context.Comments.Update(comment);
        await context.SaveChangesAsync();
    }

    public async Task<CommentEntity> GetByIdAsync(Guid commentId)
    {
        await using var context = _contextFactory.CreateDbContext();

        return await context.Comments.FirstOrDefaultAsync(c => c.CommentId == commentId) 
               ?? throw new Exception("Nor Found!");
    }

    public async Task DeleteAsync(Guid commentId)
    {
        await using var context = _contextFactory.CreateDbContext();

        var comment = await GetByIdAsync(commentId);

        context.Remove(comment);
        await context.SaveChangesAsync();
    }

    #endregion
}