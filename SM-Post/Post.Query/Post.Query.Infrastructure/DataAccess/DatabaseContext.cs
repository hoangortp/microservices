using Microsoft.EntityFrameworkCore;
using Post.Query.Domain.Entities;

namespace Post.Query.Infrastructure.DataAccess
{
    public class DatabaseContext : DbContext
    {
        #region Constructors

        public DatabaseContext(DbContextOptions options) : base(options)
        {
        }

        #endregion

        #region Properties

        public DbSet<PostEntity> Posts { get; set; }

        public DbSet<CommentEntity> Comments { get; set; }

        #endregion
    }
}