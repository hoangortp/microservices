using Microsoft.EntityFrameworkCore;

namespace Post.Query.Infrastructure.DataAccess
{
    public class DatabaseContextFactory
    {
        #region Fields

        private readonly Action<DbContextOptionsBuilder> _configureDbContext;

        #endregion

        #region Constructors

        public DatabaseContextFactory(Action<DbContextOptionsBuilder> configureDbContext)
        {
            _configureDbContext = configureDbContext;
        }

        #endregion

        #region Methods

        public DatabaseContext CreateDbContext()
        {
            DbContextOptionsBuilder<DatabaseContext> optionsBuilder = new();

            _configureDbContext(optionsBuilder);

            return new DatabaseContext(optionsBuilder.Options);
        }

        #endregion
    }
}