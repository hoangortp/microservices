﻿using CQRS.Core.Queries;

namespace Post.Query.Api.Queries;

public class FindPostByIdQuery : BaseQuery
{
    #region Properties

    public Guid Id { get; set; }

    #endregion
}