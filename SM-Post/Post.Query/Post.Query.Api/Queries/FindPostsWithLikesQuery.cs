﻿using CQRS.Core.Queries;

namespace Post.Query.Api.Queries;

public class FindPostsWithLikesQuery : BaseQuery
{
    #region Properties

    public int NumberOfLikes { get; set; }

    #endregion
}