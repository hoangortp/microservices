﻿using CQRS.Core.Queries;

namespace Post.Query.Api.Queries;

public class FindPostsByAuthorQuery : BaseQuery
{
    #region Properties

    public string Author { get; set; } = default!;

    #endregion
}