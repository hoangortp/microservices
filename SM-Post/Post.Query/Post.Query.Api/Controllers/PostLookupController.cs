﻿using CQRS.Core.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Post.Common.DTOs;
using Post.Query.Api.DTOs;
using Post.Query.Api.Queries;
using Post.Query.Domain.Entities;

namespace Post.Query.Api.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class PostLookupController : ControllerBase
{
    #region Fields

    private readonly ILogger<PostLookupController> _logger;
    private readonly IQueryDispatcher<PostEntity> _queryDispatcher;

    #endregion

    #region Constructors

    public PostLookupController(ILogger<PostLookupController> logger, IQueryDispatcher<PostEntity> queryDispatcher)
    {
        _logger = logger;
        _queryDispatcher = queryDispatcher;
    }

    #endregion

    #region Methods

    [HttpGet]
    public async Task<ActionResult> GetAllPostsAsync()
    {
        try
        {
            var posts = await _queryDispatcher.SendAsync(new FindAllPostsQuery());

            return SuccessResponse(posts);
        }
        catch (Exception e)
        {
            const string SAFE_ERROR_MESSAGE = "Error occurs while retrieving all posts!";

            return ErrorResponse(e, SAFE_ERROR_MESSAGE);
        }
    }

    [HttpGet("byId/{id}")]
    public async Task<ActionResult> GetPostByIdAsync(Guid id)
    {
        try
        {
            var posts = await _queryDispatcher.SendAsync(new FindPostByIdQuery { Id = id });

            return SuccessResponse(posts);
        }
        catch (Exception e)
        {
            const string SAFE_ERROR_MESSAGE = "Error occurs while retrieving the post   !";

            return ErrorResponse(e, SAFE_ERROR_MESSAGE);
        }
    }

    [HttpGet("byAuthor/{author}")]
    public async Task<ActionResult> GetPostsByAuthorAsync(string author)
    {
        try
        {
            var posts = await _queryDispatcher.SendAsync(new FindPostsByAuthorQuery { Author = author });

            return SuccessResponse(posts);
        }
        catch (Exception e)
        {
            const string SAFE_ERROR_MESSAGE = "Error occurs while retrieving posts by author!";

            return ErrorResponse(e, SAFE_ERROR_MESSAGE);
        }
    }

    [HttpGet("withComments")]
    public async Task<ActionResult> GetPostsWithCommentsAsync()
    {
        try
        {
            var posts = await _queryDispatcher.SendAsync(new FindPostsWithCommentsQuery());

            return SuccessResponse(posts);
        }
        catch (Exception e)
        {
            const string SAFE_ERROR_MESSAGE = "Error occurs while retrieving posts with comments!";
            return ErrorResponse(e, SAFE_ERROR_MESSAGE);
        }
    }

    [HttpGet("withLikes/{numberOfLikes}")]
    public async Task<ActionResult> GetPostsWithLikesAsync(int numberOfLikes)
    {
        try
        {
            var posts = await _queryDispatcher.SendAsync(new FindPostsWithLikesQuery { NumberOfLikes = numberOfLikes });

            return SuccessResponse(posts);
        }
        catch (Exception e)
        {
            const string SAFE_ERROR_MESSAGE = "Error occurs while retrieving posts with likes!";
            return ErrorResponse(e, SAFE_ERROR_MESSAGE);
        }
    }

    private ActionResult ErrorResponse(Exception e, string safeErrorMessage)
    {
        _logger.LogError(e, safeErrorMessage);

        return StatusCode(StatusCodes.Status500InternalServerError, new BaseResponse
        {
            Message = safeErrorMessage
        });
    }

    private ActionResult SuccessResponse(List<PostEntity> posts)
    {
        if (!posts.Any())
        {
            return NoContent();
        }

        var count = posts.Count;

        return Ok(new PostLookupResponse
        {
            Posts = posts,
            Message = $"Retrieve post{(count > 1 ? "s" : string.Empty)} successfully!"
        });
    }

    #endregion
}