﻿using Post.Common.DTOs;
using Post.Query.Domain.Entities;

namespace Post.Query.Api.DTOs;

public class PostLookupResponse : BaseResponse
{
    #region Properties

    public List<PostEntity> Posts { get; set; } = [];

    #endregion
}