using CQRS.Core.Events;

namespace CQRS.Core.Domain
{
    public abstract class AggregateRoot
    {
        #region Fields
        
        protected Guid _id;

        private readonly List<BaseEvent> _changes = [];
        
        #endregion

        #region Properties
        
        public Guid Id => _id;

        public int Version { get; set; } = -1;
        
        #endregion

        #region Methods
        
        /// <summary>
        /// Get all uncommited changes of aggregate root
        /// </summary>
        public IEnumerable<BaseEvent> GetUncommittedChanges()
        {
            return _changes;
        }

        /// <summary>
        /// Set all changes as commited
        /// </summary>
        public void MarkChangesAsCommitted()
        {
            _changes.Clear();
        }

        /// <summary>
        /// Apply state change based on event
        /// </summary>
        /// <param name="isNew">To check if the event is new or already retrieved from event store</param>
        private void ApplyChange(BaseEvent @event, bool isNew)
        {
            var method = GetType().GetMethod("Apply", [@event.GetType()]);

            if (method is null)
            {
                throw new ArgumentNullException(nameof(method), $"The apply method is not found in the aggregate for {@event.GetType().Name}!");
            }

            method.Invoke(this, [@event]);

            if (isNew)
            {
                _changes.Add(@event);
            }
        }

        /// <summary>
        /// Raise event to make state change
        /// </summary>
        /// <param name="event">Event object to be raised</param>
        protected void RaiseEvent(BaseEvent @event)
        {
            ApplyChange(@event, true);
        }

        /// <summary>
        /// Re-use an event in a collection of events
        /// </summary>
        /// <param name="events">A collection of events to be replayed</param>
        public void ReplayEvents(IEnumerable<BaseEvent> events)
        {
            foreach (var @event in events)
            {
                ApplyChange(@event, false);
            }
        }
        
        #endregion
    }
}