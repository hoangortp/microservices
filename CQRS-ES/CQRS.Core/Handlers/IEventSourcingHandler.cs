using CQRS.Core.Domain;

namespace CQRS.Core.Handlers
{
    public interface IEventSourcingHandler<T>
    {
        #region Methods
        
        Task SaveAsync(AggregateRoot aggregate);
        
        Task<T> GetByIdAsync(Guid aggregateId);

        Task RepublishEventsAsync();

        #endregion
    }
}