﻿namespace CQRS.Core.Consumers;

public interface IEventConsumer
{
    #region Methods

    void Consume(string topic);

    #endregion
}