using CQRS.Core.Commands;

namespace CQRS.Core.Infrastructure
{
    /// <summary>
    /// This is Mediator interface that's used for registering command handler methods and dispatching commands
    /// </summary>
    public interface ICommandDispatcher
    {
        #region Methods
        
        /// <summary>
        /// This method is used for registing command handler
        /// </summary>
        /// <param name="handler">Pass in a command handler which will be an async task</param>
        void RegisterHandler<T>(Func<T, Task> handler) where T : BaseCommand;

        /// <summary>
        /// This method is used for sending/dispatching commands to registered command handler
        /// </summary>
        /// <param name="command">Pass in a concrete command</param>
        Task SendAsync(BaseCommand command);
        
        #endregion
    }
}