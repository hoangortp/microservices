using CQRS.Core.Messages;

namespace CQRS.Core.Events
{
    public abstract class BaseEvent(string type) : Message
    {
        #region Properties
        
        /// <summary>
        /// Version of message
        /// </summary>
        public int Version { get; set; }

        /// <summary>
        /// Type of message
        /// </summary>
        public string Type { get; set; } = type;
        
        #endregion
    }
}