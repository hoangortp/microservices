namespace CQRS.Core.Messages
{
    public class Message
    {
        #region Properties
        
        public Guid Id { get; set; }
        
        #endregion
    }
}