using CQRS.Core.Events;

namespace CQRS.Core.Producers
{
    public interface IEventProducer
    {
        #region Methods
        
        Task ProduceAsync<T>(string topic, T @event) where T : BaseEvent;
        
        #endregion
    }
}